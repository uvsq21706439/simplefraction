package simplefraction;

public class Fraction {
	private int nominateur;
	private int denominateur;
	
	public Fraction(int nominateur, int denominateur) {
		this.nominateur = nominateur; 
		this.denominateur = denominateur;
	}


	@Override
	public String toString() {
		return "Fraction [nominateur=" + nominateur + ", denominateur=" + denominateur + "]";
	}

}
